package sample2;

import utils.Threads;

public class Sample {

    private volatile boolean interrupted = false;

    public void run() {
        while (!interrupted) {
        }
    }

    public static void main(String[] args) {
        final Sample sample = new Sample();
        final Thread thread = new Thread(() -> {
            Threads.sleep(1000);
            System.out.println("try to interrupt");
            sample.interrupted = true;
        });
        thread.start();
        sample.run();
    }
}
