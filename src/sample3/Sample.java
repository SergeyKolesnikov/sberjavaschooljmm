package sample3;

import utils.Threads;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Sample {

    private static volatile boolean started = false;

    private Point point;

    public static void main(String[] args) throws InterruptedException {
        new Sample().run();
    }

    void run() throws InterruptedException {
        final Object dummy = new Object();
        final Map<Integer, Object> results = new ConcurrentHashMap<>();
        System.out.println("start in " + Thread.currentThread().getName());
        for (int i = 0; i < 100; i++) {
            started = false;
            point = new Point(1, 1);
            final Thread thread1 = new Thread(() -> {
                while (!started) {
                    Thread.onSpinWait();
                }
                point = new Point(2, 2);
            });
            final Thread thread2 = new Thread(() -> {
                while (!started) {
                    Thread.onSpinWait();
                }
                int a = point.x;
                results.put(a, dummy);
            });
            thread1.start();
            thread2.start();
            started = true;
            Threads.join(thread1, thread2);
            if (i % 100 == 0)
                System.out.println("i = " + i);
        }
        System.out.println("end in " + Thread.currentThread().getName());
        System.out.println("\n" + results.keySet().toString());
    }
}

class Point {

    int x;
    int y;

    public Point(int x, int y) {
//        for (int i = 0; i < 100000000; i++) {}
        this.x = x;
        this.y = y;
    }
}
