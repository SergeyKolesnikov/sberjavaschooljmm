package sample6;

import utils.Threads;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

public class Sample {

    private static volatile boolean started = false;

    public static void main(String[] args) {
        final var dummy = new Object();
        final Map<String, Object> results = new ConcurrentHashMap<>();
        Runnable executable = () -> {
            while (!started) {}
            results.put(Instance.get().id, dummy);
        };

        for (int j = 0; j < 10000; j++) {
            started = false;
            Instance.instance = null;
            results.clear();
            final Thread[] threads = IntStream.range(0, 10).mapToObj(i -> new Thread(executable)).peek(Thread::start).toArray(Thread[]::new);
            started = true;
            Threads.join(threads);
            System.out.println(results.keySet());
        }
    }
}

class Instance {

    static Instance instance;

    static Instance get() {
        if (instance == null) {
            synchronized (Instance.class) {
                if (instance == null)
                    instance = new Instance();
            }
        }
        return instance;
    }

    final String id;

    Instance() {
        id = UUID.randomUUID().toString();
    }
}
