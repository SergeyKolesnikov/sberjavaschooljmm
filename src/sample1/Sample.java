package sample1;

import utils.Threads;

import java.util.HashSet;
import java.util.Set;

public class Sample {

    private volatile boolean started = false;

    private int x = 0;
    private int y = 0;

    private int a;
    private int b;


    public static void main(String[] args) {

        final Set<String> results = new HashSet<>();

        for (int i = 0; i < 10_000; i++) {
            final Sample sample = new Sample();

            final Thread thread1 = new Thread(() -> {
                while (!sample.started) {Thread.yield();}
                sample.a = sample.x;
                sample.y = 1;
            });

            final Thread thread2 = new Thread(() -> {
                while (!sample.started) {Thread.yield();}
                sample.b = sample.y;
                sample.x = 1;
            });

            thread1.start();
            thread2.start();
            sample.started = true;
            Threads.join(thread1, thread2);

            final String str = sample.print();
            results.add(str);
//            System.out.println(str);
        }
        System.out.println("\n" + results.toString());
    }

    private String print() {
        return "a=" + a + " b=" + b;
    }
}
