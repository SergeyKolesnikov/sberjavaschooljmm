package sample5;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Sample {
    private static Sample instance;

    static Sample getInstance() {
        if (instance == null) {
            synchronized (Sample.class) {
                if (instance == null) {
                    instance = new Sample();
                    instance.value = Long.MAX_VALUE - 100;
                }
            }
        }
        return instance;
    }

    private long value;

    private Sample() {
        this.value = 1;
    }

    private static volatile boolean started = false;

    public static void main(String[] args) {
        final ExecutorService executorService = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 1000; i++) {
            executorService.execute(() -> {
                while (!started) {}
                System.out.println(getInstance().value);
            });
        }
        started = true;
        executorService.shutdown();
    }
}
