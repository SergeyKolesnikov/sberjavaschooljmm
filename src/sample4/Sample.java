package sample4;

import utils.Threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Sample {

    private static Sample instance;

    static Sample getInstance() {
        if (instance == null) {
            synchronized (Sample.class) {
                if (instance == null) {
                    instance = new Sample();
                    Threads.sleep(1);
                    instance.value = 10;
                }
            }
        }
        return instance;
    }

    private int value;

    private Sample() {
        this.value = 1;
    }

    private static volatile boolean started = false;

    public static void main(String[] args) {
        final ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            executorService.execute(() -> {
                while (!started) Thread.onSpinWait();
                System.out.println(getInstance().value);
                System.out.println("" + System.currentTimeMillis() + " " + Thread.currentThread().getName());
            });
        }
        started = true;
        executorService.shutdown();
    }
}
